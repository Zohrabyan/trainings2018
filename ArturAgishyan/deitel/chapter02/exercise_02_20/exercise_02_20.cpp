#include <iostream>

int
main()
{
    int radius;
    std::cout << "Type radius of a circle: ";
    std::cin >> radius;  
    if (radius <= 0) {
        std::cout << "Error1: The radius of a circle cannot be minus and zero." << std::endl;
        return 1;
    }
    std::cout << "Circle's diameter is " << radius * 2 << std::endl;
    std::cout << "Circumference is " << (2 * 3.14159 * radius) << std::endl;
    std::cout << "Area is " << (3.14159 * radius * radius) << std::endl;

    return 0;
}
