#include "Invoice.hpp"
#include <iostream>

Invoice::Invoice(std::string partNumber, std::string partDescription, int itemQuantity, int price)
{
    setPartNumber(partNumber);
    setPartDescription(partDescription);
    setQuantity(itemQuantity);
    setUnitPrice(price);
}

void
Invoice::setPartNumber(std::string partNumber)
{
    partNumber_ = partNumber;
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

void
Invoice::setPartDescription(std::string partDescription)
{
    partDescription_ = partDescription;
}

std::string
Invoice::getPartDescription()
{
    return partDescription_;
}

void
Invoice::setQuantity(int itemQuantity)
{
    if (itemQuantity < 0) {
        std::cout << "Warning 1: The quantity is set to 0 as it cannot be a negative number!\n";     
        quantity_ = 0;
	return;
    }
    quantity_ = itemQuantity;
}

int
Invoice::getQuantity()
{
    return quantity_;
}

void
Invoice::setUnitPrice(int price)
{
    if (price < 0) {
        std::cout << "Warning 2: The price is set to 0 as it cannot be negative!\n";
        unitPrice_ = 0;
	return;
    }
    unitPrice_ = price;
}

int
Invoice::getUnitPrice()
{
    return unitPrice_;
}

int
Invoice::getInvoiceAmount()
{
    return quantity_ * unitPrice_;
}

