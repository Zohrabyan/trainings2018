#include <string>

class Invoice
{
public:
    Invoice(std::string pNumber, std::string partDescription, int itemQuantity, int price);
    void setPartNumber(std::string partNumber);
    std::string getPartNumber();
    void setPartDescription(std::string partDescription);
    std::string getPartDescription();
    void setQuantity(int itemQuantity);
    int getQuantity();
    void setUnitPrice(int price);
    int getUnitPrice();
    int getInvoiceAmount();

private:
    std::string partNumber_;
    std::string partDescription_;
    int quantity_;
    int unitPrice_;
};

