#include <string>

class Employee
{
public:
    Employee(std::string name, std::string surname, int salary);
    void setFirstName(std::string name);
    std::string getFirstName();
    void setLastName(std::string surname);
    std::string getLastName();
    void setSalary(int salary);
    int getSalary();

private:
    std::string firstName_;
    std::string lastName_;
    int monthlySalary_;
};

