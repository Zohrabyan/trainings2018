#include "Employee.hpp"
#include <iostream>

Employee::Employee (std::string name, std::string surname, int salary)
{
    setFirstName(name);
    setLastName(surname);
    setSalary(salary);
}

void
Employee::setFirstName(std::string name)
{
    firstName_ = name;
}

std::string
Employee::getFirstName()
{
    return firstName_;
}

void
Employee::setLastName(std::string surname)
{
    lastName_ = surname;
}

std::string
Employee::getLastName()
{
    return lastName_;
}

void
Employee::setSalary(int salary)
{
    if(salary < 0) {
        std::cout << "Warning 1: The salary is set to 0 as it cannot be negative!\n";
        monthlySalary_ = 0;
        return;	
    }
    monthlySalary_ = salary;
}

int
Employee::getSalary()
{
    return monthlySalary_;
}

