3.5 Explain the difference between a function prototype and a function definition.

We can define the function prototype as a declaration that tells the compiler the name of the function, its return type and the type of function's parameters. The type of parameters are needed for the compiler to know how much memory to reserve, but the names of that parameters are not required during prototyping.

In the function definition, we must include function's return type followed by its name which is usually lowercase. After the parentheses that follow function name, we have an opening and closing braces. Finally, we have the function's body between that braces which are statements that perform the function's task. Depending on the function's return type it may or may not return a value.

So, generally, we can say that a function definition includes function declaration and function body.
