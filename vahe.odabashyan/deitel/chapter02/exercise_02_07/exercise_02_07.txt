Discuss the meaning of each of the following objects: 
1. std::cin - the standard input stream is used to input data usually from the keyboard
2. std::cout - the standard output stream outputs data and it is usually
connected to the screen

Both "cin" and "cout" are names from the namespace std (the "std::" before
"cin" and "cout" indicates that). 
