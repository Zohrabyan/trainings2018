/// A program that separates thedigits of a five-digit number

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{ 
    int abcde;	
    std::cout << "Enter a five-digit number: ";
    std::cin >> abcde;
    std::cout << abcde / 10000 << "   " << (abcde / 1000) % 10 << "   " << (abcde / 100) % 10 << "   " << (abcde % 100) / 10 << "   " << abcde % 10 << std::endl;

    return 0; /// program completed successfully
} /// end function main

