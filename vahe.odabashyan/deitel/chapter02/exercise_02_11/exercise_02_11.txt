Fill in the blanks in each of the following: 
1.What arithmetic operations are on the same level of precedence as
multiplication? Division and Modulus.

2.When parentheses are nested, which set of parentheses is evaluated first in
an arithmetic expression? The inner most pair of parentheses.

3.A location in the computer's memory that may contain different values at
various times throughout the execution of a program is called a variable.
