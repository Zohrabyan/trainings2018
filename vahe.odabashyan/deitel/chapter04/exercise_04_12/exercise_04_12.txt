What does the following program print? 

 1  // Exercise 4.12: ex04_12.cpp
 2  // What does this program print?
 3  #include <iostream>
 4  using std::cout;
 5  using std::endl;
 6
 7  int main()
 8  {
 9     int y; // declare y
10     int x = 1; // initialize x
11     int total = 0; // initialize total
12
13     while ( x <= 10 ) // loop 10 times
14     {
15        y = x * x; // perform calculation
16        cout << y << endl; // output result
17        total += y; // add y to total
18        x++; // increment counter x
19     } // end while
20
21     cout << "Total is " << total << endl; // display result
22     return 0; // indicate successful termination
23  } // end main


The above program loops ten times, calculates and prints the squares of the numbers 1 through 10 inclusive (1, 4, 9, 16, 25, 36, 49, 64, 81, 100). With each iteration, the new value of "y" is added to the total, until the value of "x" becomes 11 and the loop terminates. Finally, the sum of squared values is printed: "Total is 385".
