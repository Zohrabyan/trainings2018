#include <iostream>

int
main()
{
    int number;

    std::cout << "Enter a number: ";
    std::cin  >> number; 
    
    if (number < 10000){
        std::cout << "Error 1: The number can't be smaller than 10000." << std::endl;
        return 1;
    }
 
    if (number > 99999){
        std::cout << "Error 1: The number can't be bigger than 99999." << std::endl;
        return 1;
    }
   
    int a = number / 10000;
    int b = (number / 1000) % 10;
    int c = (number / 100) % 10;
    int d = (number / 10) % 10;
    int e = number % 10;
    
    std::cout << a << "   " << b << "   " << c << "   " << d << "   " << e << std::endl;
      
    return 0;
}
