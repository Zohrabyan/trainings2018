#include "headers/core/ChatServer.hpp"
#include "headers/core/ChatConnectionListener.hpp"
#include "headers/core/ChatConnection.hpp"

int
ChatServer::execute()
{
    ChatConnectionListener connectionListener(this);
    return 0;
}

void
ChatServer::addConnection(ChatConnection* connection)
{
    connections_.push_back(connection);
}

void
ChatServer::sendToChatRoom(const char* message, const int messageSize)
{
    typedef ChatConnections::const_iterator ConnIt;
    for (ConnIt it = connections_.begin(); it != connections_.end(); ++it) {
        (*it)->send(message, messageSize);
    }
}

