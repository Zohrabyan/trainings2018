#ifndef __SOME_CLASS__
#define __SOME_CLASS__

class SomeClassImpl;

class SomeClass
{
public:
    SomeClass();
    ~SomeClass();
    void someFunction();
private:
    SomeClassImpl* impl_;
};

#endif /// __SOME_CLASS_

