#include "SomeClass.hpp"
#include "SomeClassImpl.hpp"

SomeClass::SomeClass()
    : impl_(new SomeClassImpl(9))
{
}

SomeClass::~SomeClass()
{
    delete impl_;
}

void
SomeClass::someFunction()
{
    impl_->someFunction();
}

