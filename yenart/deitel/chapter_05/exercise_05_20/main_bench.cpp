#include <benchmark/benchmark.h>
#include <cstdlib>
///#include <iostream>


template<typename FV>
static void
BM_function_runner(benchmark::State& state) {
    const int maxSide = state.range(0);
    FV fv;
    int count = 0;
    while (state.KeepRunning()) {
	benchmark::DoNotOptimize(count = fv.find_pyth_triples(maxSide));
        ///std::cout << count << std::endl;
    }
    ///state.SetItemsProcessed(count);
}

struct V1 {
    int find_pyth_triples(const int maxSide) {
        int count = 0;
        for (int firstSide = 1; firstSide <= maxSide; ++firstSide) {
            for (int secondSide = 1; secondSide <= maxSide; ++secondSide) {
                for (int hypotenuse = 1; hypotenuse <= maxSide; ++hypotenuse) {
                    ++count;
                    if (hypotenuse * hypotenuse == firstSide * firstSide + secondSide * secondSide) {
                        ///std::cout << firstSide  << " "
                        ///          << secondSide << " "
                        ///          << hypotenuse << std::endl;
                    }
                }
            }
        }
        return count;
    }
};

struct V2 {
    int find_pyth_triples(const int maxSide) {
        int count = 0;
        for (int firstSide = 3; firstSide <= maxSide; ++firstSide) {
            const int firstSideSquare = firstSide * firstSide;
            for (int secondSide = firstSide; secondSide <= maxSide; ++secondSide) {
                const int secondSideSquare = secondSide * secondSide;
                const int squareSum = firstSideSquare + secondSideSquare;
                for (int hypotenuse = secondSide + 1; hypotenuse <= maxSide; ++hypotenuse) {
                    ++count;
                    if (hypotenuse * hypotenuse == squareSum) {
                    }
                }
            }
        }
        return count;
    }
};

struct V3 {
    int find_pyth_triples(const int maxSide) {
        int count = 0;
        for (int firstSide = 3; firstSide <= maxSide; ++firstSide) {
            const int firstSideSquare = firstSide * firstSide;
            for (int secondSide = firstSide; secondSide <= maxSide; ++secondSide) {
                const int secondSideSquare = secondSide * secondSide;
                const int squareSum = firstSideSquare + secondSideSquare;
                const int sideSum = firstSide + secondSide;
                for (int hypotenuse = secondSide + 1; hypotenuse < sideSum && hypotenuse <= maxSide; ++hypotenuse) {
                    ++count;
                    if (hypotenuse * hypotenuse == squareSum) {
                    }
                }
            }
        }
        return count;
    }
};

struct V4 {
    int find_pyth_triples(const int maxSide) {
        int count = 0;
        for (int firstSide = 3; firstSide <= maxSide; ++firstSide) {
            const int firstSideSquare = firstSide * firstSide;
            for (int secondSide = firstSide; secondSide <= maxSide; ++secondSide) {
                const int secondSideSquare = secondSide * secondSide;
                const int squareSum = firstSideSquare + secondSideSquare;
                const int sideSum = firstSide + secondSide;
                const int maxHypotenuse = std::min(sideSum, maxSide);
                for (int hypotenuse = secondSide + 1; hypotenuse < maxHypotenuse; ++hypotenuse) {
                    ++count;
                    if (hypotenuse * hypotenuse == squareSum) {
                    }
                }
            }
        }
        return count;
    }
};

const char* maxSideStr = ::getenv("MAX_SIDE");
const int maxSide = maxSideStr != NULL ? ::atoi(maxSideStr) : 500;

BENCHMARK_TEMPLATE1(BM_function_runner, V1)->Arg(maxSide);
BENCHMARK_TEMPLATE1(BM_function_runner, V2)->Arg(maxSide);
BENCHMARK_TEMPLATE1(BM_function_runner, V3)->Arg(maxSide);
BENCHMARK_TEMPLATE1(BM_function_runner, V4)->Arg(maxSide);

BENCHMARK_MAIN();

