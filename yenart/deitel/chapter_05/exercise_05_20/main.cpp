#include <iostream>
#include <chrono>
#include <thread>

class Timer
{
public:
    Timer(const std::string& message = "")
        : start_(std::chrono::system_clock::now())
        , message_(message)
        {
    }
    ~Timer() {
        std::chrono::_V2::system_clock::time_point end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsedSeconds = end - start_;
        std::cout << (message_.empty() ? "Elapsed seconds: " : message_);
        std::cout << elapsedSeconds.count() * 1000 << std::endl;
    }
private:
    std::chrono::_V2::system_clock::time_point start_;
    std::string message_;
};

void
find_pyth_triples(const int maxSide)
{
    Timer timer("Elapsed time of find_pyth_triples: ");
    for (int firstSide = 3; firstSide <= maxSide; ++firstSide) {
        const int firstSideSquare = firstSide * firstSide;
        for (int secondSide = firstSide; secondSide <= maxSide; ++secondSide) {
            const int secondSideSquare = secondSide * secondSide;
            const int squareSum = firstSideSquare + secondSideSquare;
            const int sideSum = firstSide + secondSide;
            const int maxHypotenuse = std::min(sideSum, maxSide);
            for (int hypotenuse = secondSide + 1; hypotenuse < maxHypotenuse; ++hypotenuse) {
                if (hypotenuse * hypotenuse == squareSum) {
                    ///std::cout << firstSide  << " "
                    ///          << secondSide << " "
                    ///          << hypotenuse << std::endl;
                }
            }
        }
    }
}

int
main()
{
    const char* threadCountStr = ::getenv("THREAD_COUNT");
    const int threadCount = threadCountStr != NULL ? ::atoi(threadCountStr) : 2;
    {
        Timer timer("Overall Sequental Elapsed Time: ");
        for (int i = 0; i < threadCount; ++i) {
            find_pyth_triples(500);
        }
    }
    {
        Timer timer("Overall Parallel Elapsed Time: ");
        std::thread t[threadCount];
        for (int i = 0; i < threadCount; ++i) {
            t[i] = std::thread(find_pyth_triples, 500);
        }

        for (int i = 0; i < threadCount; ++i) {
            t[i].join();
        }
    }
    return 0;
}

