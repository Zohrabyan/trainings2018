<!DOCTYPE html>
<?php
    function createNameAndInputsTable() {
        $salaryOfManager = $_POST["salaryOfManager"]; 
        $hourlyWorker = $_POST["hourlyWorker"];
        $time = $_POST["time"];
        $commissionWorker = $_POST["commissionWorker"];
        $pieceWorker = $_POST["pieceWorker"];
        $amount = $_POST["amount"];
    
        if($_POST['submit']){
            $inputs = array($salaryOfManager, $hourlyWorker, $time, $commissionWorker, $pieceWorker, $amount);
            $isInputOk = TRUE;
            foreach ($inputs as $element){ 
                if (preg_match('~[^0-9]~',$element) || $element < 0 || empty($element)){
                    echo '<h2>Error 1: All inputs should be numbers and above 0.</h2>';
                    $isInputOk = FALSE;
                    break;
                }    
            }   
            if (TRUE == $isInputOk){
                $salaryOfHourlyWorker = $hourlyWorker * $time;
                if ($time > 40){
                    $salaryOfHourlyWorker = (1.5 - 20.0 / $time);
                }       
                $salaryOfCommsionWorker = $commissionWorker * 0.057 + 250;
                $salaryOfPieceWorker = $amount * $pieceWorker;      

                $computedSalaries = array($salaryOfManager, $salaryOfHourlyWorker, $salaryOfCommsionWorker, $salaryOfPieceWorker);
            }    
        }
    
        $workerTypes = array('Manager', 'Hourly Worker', 'Commission Worker', 'Piece Worker');
        $workerColumns = array(
            array('Fixed salary' => 'salaryOfManager'),
            array('Fixed salary of hourly worker' => 'hourlyWorker', 'Work time' => 'time'),
            array('Salary of a week' => 'commissionWorker'),
            array('Fixed salary of piece worker' => 'pieceWorker', 'Amount of produced items' => 'amount')
            );
        echo '<table>';
        echo    '<tr>';
            foreach ($workerTypes as $element){
                echo '<td><center>Salary of '.$element.'</center></td>';
            }
        echo    '</tr>';    
        echo    '<tr>';
            foreach ($workerColumns as $name){
                echo '<td><center>';
                foreach ($name as $key => $input){
                    echo $key.'<br/><br/><input name="'.$input.'"><br/><br/>';
                }
                echo '</center></td>';
            }
        echo    '</tr>';
        echo    '<tr>';
            foreach ($computedSalaries as $salary){
                echo '<td><center>'.$salary.'</center></td>';
            }
        echo    '</tr>';
        echo '</table>';    
    }
    
    echo '<html>';
    echo    '<head>';
    echo        '<meta charset="UTF-8">';
    echo        '<title>Deitel - Exercise 5.21 - PHP Implementation</title>';
    echo        '<link rel="stylesheet" type="text/css" href="./css/exercise_05_21.css">';
    echo    '</head>';
    echo    '<body bgcolor="#ffff00" >';
    echo        '<center>';
    echo            '<h3>Deitel - Exercise 5.21 - PHP Implementation</h3>';
    echo            '<form action="" method="post">';    
    echo                '<input type="submit" name="submit" value="Submit"><br/><br/>';
    createNameAndInputsTable();
    echo            '</form>';
    echo        '</center>';
    echo    '</body>';
    echo '</html>';  
?>
