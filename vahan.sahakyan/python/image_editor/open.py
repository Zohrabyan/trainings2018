import sys;
import cv2;
import matplotlib.pyplot as plt;

def openImage(fileName):
    image = fileName;
    img = cv2.imread(image, 1);
    if type(img) == type(None):
        print("Error 1: Cannot find " + fileName);
        exit(1);

    plt.imshow(img, cmap = 'gray', interpolation = "bicubic");
    plt.xticks([]);
    plt.yticks([]);
    plt.show();
    #cv2.waitKey(0);
    #cv2.destroyAllWindows();

    #cv2.imwrite("messi5gray.jpg", img);

def usage():
    print("Usage");
    print("\tpython open.py <filename>");

def main():
    if len (sys.argv) < 2:
        usage();
        return;

    fileName = sys.argv[1];
    openImage(fileName);

if __name__ == "__main__":
    main();

