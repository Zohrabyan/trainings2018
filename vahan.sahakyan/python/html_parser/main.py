import sys;
import bs4;
import urllib2;
import os;

def downloadImage(baseUrl, imageUrl):
    url = os.path.join(baseUrl + imageUrl);
    imageDir = os.path.dirname(imageUrl);
    if not os.path.exists(imageDir):
        os.makedirs(imageDir);
    urlHandle = urllib2.urlopen(url);
    with open(imageUrl, "w") as fh:
        fh.write(urlHandle.read());
    
def downloadImages(url):
    urlHandle = urllib2.urlopen(url);
    soup = bs4.BeautifulSoup(urlHandle, "html.parser");
    for img in soup.find_all("img"):
        if u"src" in img.attrs:
            downloadImage(url, img.attrs[u"src"]);


def usage():
    print("Usage")
    print("\tpython main.py <url>");

def main():
    if len(sys.argv) < 2:
        usage();
        return;

    url = sys.argv[1];
    downloadImages(url);

if __name__ == "__main__":
    main();


