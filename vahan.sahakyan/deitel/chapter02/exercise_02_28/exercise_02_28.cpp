#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter a five-digit number: ";
    std::cin >> number;

    if (number < 10000) {
        std::cout << "Error 1: The number is smaller than required." << std::endl;
        return 1;
    }
    if (number > 99999) {
        std::cout << "Error 2: The number is bigger than required." << std::endl;
        return 2;
    }

    std::cout << (number / 10000) << "   " << (number / 1000) % 10 << "   " << (number / 100) % 10 << "   " << (number / 10) % 10 << "   " << number % 10 << std::endl;
    return 0;
}

