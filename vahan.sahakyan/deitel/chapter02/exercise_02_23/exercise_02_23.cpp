#include <iostream>

int
main ()
{
    int number1, number2, number3, number4, number5;
    std::cout << "Insert five numbers: ";
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;
    
    int smallest = number1;
    if (smallest > number2) {
        smallest = number2;
    }
    if (smallest > number3) {
        smallest = number3;
    }
    if (smallest > number4) {
        smallest = number4;
    }
    if (smallest > number5) {
        smallest = number5;
    }

    int largest = number1;
    if (largest < number2) {
        largest = number2;
    }
    if (largest < number3) {
        largest = number3;
    }
    if (largest < number4) {
        largest = number4;
    }
    if (largest < number5) {
        largest = number5;
    }

    std::cout << "Smallest is " << smallest << std::endl;
    std::cout << "Largest is " << largest << std::endl;

    return 0;
}

