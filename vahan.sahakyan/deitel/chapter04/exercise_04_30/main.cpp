#include <iostream>
#include <unistd.h>

/// functions' declerations
double diameter(double number);
double circumference(double number);
double area(double number);

int
main()
{
    double radius;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Circle radius: ";
    }
    std::cin >> radius;
    if (radius <= 0) {
        std::cerr << "Error 1: Radius must be positive\n";
        return 1;
    }

    std::cout << "Diameter: " << diameter(radius) << std::endl;
    std::cout << "Circumference: " << circumference(radius) << std::endl;
    std::cout << "Area: " << area(radius) << std::endl;

    return 0;
}

/// functions' definitions
double
diameter(double number)
{
    return number * 2;
}

double
circumference(double number)
{
    return (number * 2) * 3.14159;
}

double
area(double number)
{
    return (number * number) * 3.14159;
}

