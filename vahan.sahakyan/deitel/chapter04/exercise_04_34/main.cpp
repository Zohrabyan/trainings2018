#include <iostream>
#include <unistd.h>

int error1();
int error2();
int encrypt(int data);
int decrypt(int data);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert 4-digit number to process: ";
    }
    int data;
    std::cin >> data;

    if (data > 9999 || data < 1000) {
        return error1();
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Actions:"
                  << "\n1.Encryption"
                  << "\n2.Decryption"
                  << "\n\nChoose an action: ";
    }
    int choice;
    std::cin >> choice;

    switch (choice) {
    case 1:  return encrypt(data);
    case 2:  return decrypt(data);
    default: return error2();
    }

    return 0;
}

int
encrypt(int data)
{
    int d1 = ((data / 1000) + 7) % 10;
    int d2 = ((data % 1000 / 100) + 7) % 10;
    int d3 = ((data % 100 / 10) + 7) % 10;
    int d4 = ((data % 10) + 7) % 10;

    std::cout << "\nEncrypted data: " << d3 << d4 << d1 << d2 << std::endl;
    return 0;
}

int
decrypt(int data)
{
    int d1 = ((data / 1000) + 3) % 10;
    int d2 = ((data % 1000 / 100) + 3) % 10;
    int d3 = ((data % 100 / 10) + 3) % 10;
    int d4 = ((data % 10) + 3) % 10;

    std::cout << "\nDecrypted data: " << d3 << d4 << d1 << d2 << std::endl;
    return 0;
}

int
error1()
{
    std::cout << "\nERROR 1: Data is non 4-digit!" << std::endl;
    return 1;
}

int
error2()
{
    std::cout << "\nERROR 2: Wrong choice" << std::endl;
    return 2;
}

