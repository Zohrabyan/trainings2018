#include <iostream>
#include <unistd.h>

int
main()
{
    if(::isatty(STDIN_FILENO)) {
        std::cout << "Enter side size: ";
    }
    int side;
    std::cin >> side;
    if (side <= 0) {
        std::cerr << "ERROR 1: Wrong side size!\n";
        return 1;
    }
    int row = 1;
    while (row <= side) {
        int column = 1;
        if (0 == row % 2) {
            std::cout << " ";
        }
        while (column <= side) {
            std::cout << "* ";
            ++column;
        }
        ++row;
        std::cout << std::endl;
    }
    return 0;
}
