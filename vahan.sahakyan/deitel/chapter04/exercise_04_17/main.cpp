#include <iostream>
#include <climits>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert the numbers of units sold by each salesperson (10x):\n";
    }

    int counter = 1;
    int mostUnits = INT_MIN;
    while (counter <= 10) {
        int unitsNumber;
        std::cin >> unitsNumber;
        if (mostUnits < unitsNumber) {
            mostUnits = unitsNumber;
        }
        ++counter;
    }
    std::cout << "The contest wins the salesperson who has sold " << mostUnits << " units!!!" << std::endl;

    return 0;
}

