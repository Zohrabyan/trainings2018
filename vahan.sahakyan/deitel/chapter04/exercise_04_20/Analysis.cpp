#include "Analysis.hpp"
#include <iostream>
#include <unistd.h>

void
Analysis::processExamResults()
{
    int passes = 0;
    int failures = 0;
    int studentCounter = 1;

    while (studentCounter <= 10) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter result (1 = pass, 2 = fail): ";
        }
        int result;
        std::cin >> result;
        if (1 == result) {
            ++passes;
            ++studentCounter;
        } else if (2 == result) {
            ++failures;
            ++studentCounter;
        } else {
            std::cout << "WARNING 1: Wrong result" << std::endl;
        }
    }
    std::cout << "Passed " << passes << "\nFailed " << failures << std::endl;
    if (passes > 8) {
        std::cout << "Raise tuition" << std::endl;
    }
    return;
}

