#include <iostream>
#include <unistd.h>

int
main()
{
    double totalMiles = 0;
    double totalGallons = 0;

    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter miles driven (-1 to quit): ";
        }
        double miles;
        std::cin >> miles;
        std::cout << std::fixed;
        if (-1 == miles) {
            return 0;
        }
        if (miles < 0) {
            std::cerr << "Error 1: Wrong miles!\n";
            return 1;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter gallons used: ";
        }
        double gallons;
        std::cin >> gallons;
        if (gallons <= 0) {
            std::cerr << "Error 2: Wrong gallons!\n";
            return 2;
        }

        totalMiles += miles;
        totalGallons += gallons;
        double milesPerGallon = miles / gallons;
        double totalMilesPerGallon = totalMiles / totalGallons;

        std::cout << "MPG this tankful: " << milesPerGallon;
        std::cout << "\nTotal MPG: " << totalMilesPerGallon << "\n\n";
    }

    return 0;
}

