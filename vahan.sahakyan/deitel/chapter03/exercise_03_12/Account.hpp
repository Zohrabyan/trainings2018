
class Account
{
public:
    Account(int balance);
    void credit(int credit);
    void debit(int debit);
    int getBalance();
private:
    int accountBalance_;
};

