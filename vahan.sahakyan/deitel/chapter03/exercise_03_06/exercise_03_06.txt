A default cosntructor is a constructor with no parameters.

In case if a class has only an implicitly defined default constructor, an object’s data members are to be initialized using the object’s member functions.
