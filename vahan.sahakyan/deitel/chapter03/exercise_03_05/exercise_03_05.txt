Function Prototype:
A function prototype is a declaration of a function that tells the compiler the function's name, its return type and the types of its parameters.

Function Definition:
A function definition provides the actual body of the function.
The function body contains a collection of statements that define what the function does.
