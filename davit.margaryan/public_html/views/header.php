<?php
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';

    echo '<html xmlns="http://www.w3.org/1999/xhtml">';
    echo     '<head>';
    echo         '<meta charset="UTF-8" />';
    echo         '<title>Simple Project</title>';
    echo         '<link rel="stylesheet" type="text/css" href="'.URL.'/assets/css/style.css">';
    echo     '</head>';
    echo     '<body>';
    echo         '<div class="content">';
    if (!is_null($this->get('logo'))) {
        echo         '<header>';
        foreach ($this->get('logo') as $image) {
            echo         '<img src="'.URL.'/'.$image.'" />';
        }
        echo         '</header>';
    } 
    if (!is_null($this->get('menu')) && !empty($this->get('menu'))) {
        echo         '<nav id="navBar">';
        echo             '<ul>';
        foreach ($this->get('menu') as $url => $page) {
            echo             '<a href="'.URL. '/?site='.$url.'"><li class="menu-page">'.$page.'</li></a>';
        }
        echo             '</ul>';
        echo         '</nav>';
    } 
?>
