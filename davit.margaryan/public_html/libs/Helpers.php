<?php

namespace Libs;

///////////////////////////////////////////////////////////////////
// 
// LIBRARY Helpers
//
// Helper functions
//
// public static function dd($valu, $die = false)
//
///////////////////////////////////////////////////////////////////

class Helpers
{
    /**
     * Dump and die
     *
     * @param mixed $value
     * @param bool $die
     */
    public static function dd($value, $die = false)
    {
        echo '<pre>';
        var_dump($value);
        echo '</pre>';
        if ($die) die();
    }
}
?>