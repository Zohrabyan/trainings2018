<?php

namespace Models;
use Libs\Model;

///////////////////////////////////////////////////////////////////
// 
// class Exercise extends Model
//
// Work with Exercises table in database
//
// public function __construct()
// public function get($id = NULL)
// 
// private $exercises_ = [];
//
///////////////////////////////////////////////////////////////////

class Exercise extends Model
{
    /**
     * Array of exercises
     */
    private $exercises_ = [];

    /**
     * constructor calls parent constructor and
     * gets the db connnection
     */
    function __construct()
    {
        // TODO: Save it in db
        $this->exercises_ = [
            "exercises/exercise/12" => "js - Exercise 5.12",
            "exercises/exercise/24" => "php - Exercise 5.24",
        ];
        parent::__construct();
    }

    /**
     * Return exercises from exercise table
     *
     * @param int $id - id of current exercise
     * @return array $this->exercises_ - array of exercises
     *         | current exercise which $id = id | NULL
     */
    public function get($id = NULL)
    {
        if (is_null($id)) {
            return $this->exercises_;
        }
        return NULL;
    }
}
?>
