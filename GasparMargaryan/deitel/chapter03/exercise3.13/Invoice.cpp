/// Exercise3.13: Invoice.cpp
/// Invoice member-function definitions. This file contains
/// implementations of the member functions prototyped in Invoice.hpp.
#include "Invoice.hpp" /// include definition of class Invoice
#include <iostream>

/// constructor initializes part number and description as type string and item quantity and price as type int
Invoice::Invoice(std::string partNumber, std::string partDescription, int itemQuantity, int itemPrice)
{
    setPartNumber(partNumber);
    setPartDescription(partDescription);
    setItemQuantity(itemQuantity);
    setItemPrice(itemPrice);
} /// end of Invoice constructor

/// Invoice class member function implementations
void
Invoice::setPartNumber(std::string partNumber)
{
    partNumber_ = partNumber;
};

std::string
Invoice::getPartNumber()
{
    return partNumber_;
};

void
Invoice::setPartDescription(std::string partDescription)
{
    partDescription_ = partDescription;
};

std::string
Invoice::getPartDescription()
{
    return partDescription_;
};

void
Invoice::setItemQuantity(int itemQuantity)
{
    /// in order if quantity is negative it should be set 0
    if (itemQuantity < 0) {
        itemQuantity = 0;
    }
    itemQuantity_ = itemQuantity;
};

int
Invoice::getItemQuantity()
{
    return itemQuantity_;
};

void
Invoice::setItemPrice(int itemPrice)
{
    /// in order if the price is negative it should be set 0
    if (itemPrice < 0) {
        itemPrice = 0;
    }
    itemPrice_ = itemPrice;
};

int
Invoice::getItemPrice()
{
    return itemPrice_;
};

int
Invoice::getInvoiceAmount()
{
    return Invoice::getItemQuantity() * Invoice::getItemPrice();
}; /// end of class function implementation

