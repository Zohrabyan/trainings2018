/// Exercise3.15: Date.cpp
/// Date member-function definitions. This file contains
/// implementations of the member functions prototyped in Date.hpp.
#include "Date.hpp" /// include definition of class Date
#include <iostream>

/// constructor initializes employee's first and last names as type string and the salary as type int
Date::Date(int years, int months, int days)
{
    setYear(years);
    setMonth(months);
    setDay(days);
} /// end of Date constructor

/// Date class member function implementations
void
Date::setYear(int years)
{
   years_ = years;
}

int
Date::getYear()
{
    return years_;
}

void
Date::setMonth(int months)
{
    if (months < 1) {
        months = 1;
    }
    if (months > 12) {
        months = 1;
    }
    months_ = months;
}

int Date::getMonth()
{
    return months_;
}

void
Date::setDay(int days)
{
    days_ = days;
}

int
Date::getDay()
{
    return days_;
}

void
Date::displayMessage()
{
   std::cout << Date::getDay() << "/" << Date::getMonth() << "/" << Date::getYear() << std::endl;
}
/// end of class function implementation

