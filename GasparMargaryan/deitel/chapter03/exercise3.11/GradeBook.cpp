/// Exercise3.11: GradeBook.cpp
/// GradeBook member-function definitions. This file contains
/// implementations of the member functions prototyped in GradeBook.hpp.
#include "GradeBook.hpp" /// include definition of class GradeBook
#include <iostream>

/// constructor initializes courseName and instructorName with string supplied as argument
GradeBook::GradeBook(std::string course, std::string instructor)
{
    setCourseName(course); /// call set function to initialize courseName
    setInstructorName(instructor); /// call set function to initialize instructorName
} /// end GradeBook constructor

/// function to set the course name
void
GradeBook::setCourseName(std::string name)
{
    courseName_ = name; /// store the course name in the object
} /// end function setCourseName

/// function to get the course name
std::string
GradeBook::getCourseName()
{
    return courseName_; /// return object's courseName
} /// end function getCourseName

/// function to set the instructor name
void
GradeBook::setInstructorName(std::string name)
{
    instructorName_ = name; /// store the instructor name in the object
} /// end function setInstructorName

/// function to get the instructor name
std::string
GradeBook::getInstructorName()
{
    return instructorName_; /// return object's courseName
} /// end function getCourseName

/// display a welcome message to the GradeBook user
void
GradeBook::displayMessage()
{
/// call getCourseName to get the courseName
    std::cout << "Welcome to the grade book for:\n" << getCourseName() << "!" << std::endl;
/// call getInstructorName to get the instructorName
    std::cout << "This course is presented by: " << getInstructorName() << std::endl;
} /// end function displayMessage

