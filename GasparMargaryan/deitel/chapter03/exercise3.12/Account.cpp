/// Exercise3.11: Account.cpp
/// Account member-function definitions. This file contains
/// implementations of the member functions prototyped in Account.hpp.
#include "Account.hpp" /// include definition of class Account
#include <iostream>

/// constructor initializes balance with int supplied as argument
Account::Account(int balance)
{
   if (balance < 0) {
       std::cout << "Error 1: The initial ballance is invalid. \n";
       accountBalance_ = 0;
       return;
   }
   accountBalance_ = balance;

} /// end Account constructor

void
Account::credit(int credit)
{
    accountBalance_ = accountBalance_ + credit; /// add and store the credit ammount in the balance
} /// end function credit

void
Account::debit(int debit)
{
    if (accountBalance_ < debit) {
        std::cout << "Error 2: Debit amount exceeded account balance. \n";
        return;
    }
    accountBalance_ = accountBalance_ - debit;
} /// end function debit

/// function to get account balance
int
Account::getAccountBalance()
{
    return accountBalance_;
} /// end function getAccountBalance

