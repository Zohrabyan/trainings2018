/// Exercise3.14 Employee.hpp
#include <string>
/// Invoice employee definition
class Employee
{
public:
    Employee(std::string firstName, std::string lastName, int salary); /// constructor that initializes Employee data members
/// class Employee member functions prototypes
    void setFirstName(std::string firstName);
    std::string getFirstName();

    void setLastName(std::string lastName);
    std::string getLastName();

    void setSalary(int salary);
    int getSalary();

///data member declaration
private:
    std::string firstName_;
    std::string lastName_;
    int salary_;
}; /// end class Employee

