/// Exercise5_05
/// Program that uses a "for" statement to sum a sequence of integers
#include <iostream>
#include <unistd.h>

int
main()
{
    int number;
    if (::isatty(STDIN_FILENO)) { /// prompting user to input an iteger which specifies the number of values remaining to be entered
        std::cout << "Enter positive number: " << std::endl;
    }
    std::cin >> number;
    if (number < 0) {
        std::cout << "Error 1: The number must be positive. " << std::endl;
        return 1;
    }
    int sum = 0;
    for (int counter = 1; counter <= number; ++counter) {
        if (::isatty(STDIN_FILENO)) { /// prompting user to input value
            std::cout << "Enter a value: " << std::endl;
        }
        int value;
        std::cin >> value;
        sum += value;
    }
    std::cout << "The sum of " << number << " value(s) is " << sum << std::endl;

    return 0; /// indicate successful termination
} /// end main

