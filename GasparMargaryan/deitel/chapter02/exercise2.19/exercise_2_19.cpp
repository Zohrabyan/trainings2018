/// program that inputs 3 numbers and prints the sum, average, product,
/// smallest and largest of that numbers.
#include <iostream> /// allows program to perform input and output

/// function main begins program execution
int
main()
{
    int number1; /// declare the firts integer
    int number2; /// declare the second integer
    int number3; /// declare the third integer

    std::cout << "Input three different integers: " << std::endl;
    std::cin >> number1 >> number2 >> number3;
    std::cout << "Sum is " << (number1 + number2 + number3) << std::endl;
    std::cout << "Average is " << (number1 + number2 + number3)/3 << std::endl;
    std::cout << "Product is " << (number1 * number2 * number3) << std::endl;

    /// compare and find the smallest number from inputed three integers
    int min = number1; /// declare the smallest number
    if (number2 < number1) {
        min = number2;
    }
    if (number3 < number2) {
        min = number3;
    }

    /// compare and find the largest number from inputed three integers
    int max = number3; /// declare the largest number
    if (number2 > number3) {
        max = number2;
    }
    if (number1 > number2) {
        max = number1;
    }

    std::cout << "The smallest is " << min << std::endl;
    std::cout << "The largest is " << max << std::endl;

    return 0; /// indicate that program ended successfully
} /// end function main

