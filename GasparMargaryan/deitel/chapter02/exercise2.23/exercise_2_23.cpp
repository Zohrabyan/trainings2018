/// Program that reads in five integers and determines and prints the largest
/// and the smallest integers in the group
#include <iostream> /// allows program perform input and output

/// function main begin program execution
int
main()
{
    int number1, number2, number3, number4, number5; /// declare integers to input by user
    std::cout << "Input 5 integers to compare: " << std::endl; /// prompts user to imput 5 integers
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;  /// read integers from user
    /// compare inputed integers and find the smallest one
    int min = number1; /// declare integer to store the largest smallest integers in

    if (number2 < min) {
        min = number2;
    }
    if (number3 < min) {
        min = number3;
    }
    if (number4 < min) {
        min = number4;
    }
    if (number5 < min) {
        min = number5;
    }
    std::cout << "The smallest is " << min << std::endl;

    /// compare inputed integers and find the largest one
    int max = number5; /// declare integer to store the largest smallest integers in
    if (number4 > max) {
        max = number4;
    }
    if (number3 > max) {
        max = number3;
    }
    if (number2 > max) {
        max = number2;
    }
    if (number1 > max) {
        max = number1;
    }
    std::cout << "The largest is " << max << std::endl;

    return 0; /// indicate that program ended successfully
} /// end of function main

