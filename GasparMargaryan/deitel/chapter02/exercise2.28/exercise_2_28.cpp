/// Program that inputs a five-digit integer, separates the integer into its individual
/// digits and prints the digits separated from one another by three spaces each
#include <iostream> /// allow program to perform input and output

/// function main begins program execution
int
main()
{
    int number1; /// declare five digit number
    std::cout << "Please insert 5 digit number: "; /// prompt user to enter 5 digit number
    std::cin >> number1;
    /// to be sure that inputed number is 5 digit number
    if (100000 < number1) {
        std::cout << "Error 1: the number you input should be 5 digit number. \n";
        return 1;
    }
    if (9999 > number1) {
        std::cout << "Error 1: the number you input should be 5 digit number. \n";
        return 1;
    }
    std::cout << number1 / 10000 << "   " << number1 % 10000 / 1000 << "   " << number1 % 1000 / 100 << "   " << number1 % 100 / 10 << "   " << number1 % 10 << std::endl;

    return 0; /// indicates that program finished successfully
} /// end of function main

