/// a program that obtains the two numbers from the user and prints the sum, product,
/// difference, and quotient of the two numbers
#include <iostream> /// allows program to perform input and output

/// Function main begin programs execution
int
main()
{
    /// variable declarations
    int number1; /// first integer
    int number2; /// second integer

    /// prompts user to enter the first integer
    std::cout << "Please insert the first number and press ENTER: ";
    std::cin >> number1; /// read the first integer to make arithmetic operations

    std::cout << "Please insert the second number and press ENTER: ";
    std::cin >> number2; /// read the second integer to make arithmetic operations

    std::cout << "The sum is " << number1 + number2 << std::endl;
    std::cout << "The product is " << number1 * number2 << std::endl;
    std::cout << "The difference is " << number1 - number2 << std::endl;

    if (0 == number2) {
        std::cout << "Error 1: the first number cannot be divided by 0 " << std::endl; /// 0 division reduce error function
        return 1;
    }

    std::cout << "The quotient is " << number1 / number2 << std::endl;

    return 0; /// indicate that program ended successfully
} /// end function main

