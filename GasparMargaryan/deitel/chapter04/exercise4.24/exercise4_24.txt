/// version 1, when x = 5 y = 8

if (8 == y) {
    if (5 == x) {
        std::cout << "@@@@@" << std::endl;
    }
}
else {
    std::cout << "#####" << std::endl;
}
std::cout << "$$$$$" << std::endl;
std::cout << "&&&&&" << std::endl;

output:
@@@@@
$$$$$
&&&&&

/// version 2, when x = 5 y = 8

if (8 == y) {
    if (5 == x) {
        std::cout << "@@@@@" << std::endl;
    }
}
else {
    std::cout << "#####" << std::endl;
    std::cout << "$$$$$" << std::endl;
    std::cout << "&&&&&" << std::endl;
}

output:
@@@@@

/// version 3, when x = 5, y = 8

if (8 == y) {
    if (5 == x) {
        std::cout << "@@@@@" << std::endl;
    }
}
else {
    std::cout << "#####" << std::endl;
    std::cout << "$$$$$" << std::endl;
}
std::cout << "&&&&&" << std::endl;

output:
@@@@@
&&&&&

/// version 4, when x = 5, y = 7

if (8 == y) {
   if (5 == x) {
       std::cout << "@@@@@" << std::endl;
   }
}
else {
    std::cout << "#####" << std::endl;
    std::cout << "$$$$$" << std::endl;
    std::cout << "&&&&&" << std::endl;
}

output:
#####
$$$$$
&&&&&

