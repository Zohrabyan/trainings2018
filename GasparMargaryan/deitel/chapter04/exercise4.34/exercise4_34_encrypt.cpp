/// Exercise 4.34
/// Program that input digit integer value, encrypt and decrypt it back
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please enter 4 digit number " << std::endl;
    }  /// prompt user to enter 4 digit number for encryption
    int encrypt;
    std::cin >> encrypt;
    if (encrypt < 0) { /// if entered integer is less than 0 stop the program and print error message
        /// inputed number less than 4 digit will be read as 4 digit (e.g. 999 means 0999)
        std::cout << "Error 1: Invalid number! " << std::endl;
        return 1;
    }
    if (encrypt > 9999) {
        std::cout << "Error 1: Invalid number! " << std::endl;
        return 1;
    }

    int encDigit1 = ((encrypt % 10) + 7) % 10;
    int encDigit2 = ((encrypt % 100) / 10 + 7) % 10;
    int encDigit3 = ((encrypt % 1000) / 100 + 7) % 10;
    int encDigit4 = (encrypt / 1000 + 7) % 10;

    std::cout << "Encrypted number is ";
    std::cout << encDigit2 << encDigit1 << encDigit4 << encDigit3 << std::endl;

    return 0; /// indicate successful termination
} /// end main

