 /// exercise4.20 main.cpp
 /// Test program for class Analysis.
#include "analysis.hpp" /// include definition of class Analysis

int main()
{
    Analysis application; /// create Analysis object
    application.processExamResults(); // call function to process results
    return 0; /// indicate successful termination
} /// end main

