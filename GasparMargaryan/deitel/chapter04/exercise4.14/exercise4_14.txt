Enter account number (-1 to end): 100
Enter beginning balance: 5394.78
Enter total charges: 1000.00
Enter total credits: 500.00
Enter credit limit: 5500.00
New balance is 5894.78
Account: 100
Credit limit: 5500.00
Balance: 5894.78
Credit Limit Exceeded.
Enter Account Number (or -1 to quit): 200
Enter beginning balance: 1000.00
Enter total charges: 123.45
Enter total credits: 321.00
Enter credit limit: 1500.00
New balance is 802.45
Enter Account Number (or -1 to quit): 300
Enter beginning balance: 500.00
Enter total charges: 274.73
Enter total credits: 100.00
Enter credit limit: 800.00
New balance is 674.73
Enter Account Number (or -1 to quit): -1

Algorithm
1. Initialize variables
2. Make repetition function and set sentinel value -1 to exit the program when entered
3. Prompt user to enter account number or -1 to exit the program
4. Prompt user to enter the sum of beginning balance
5. Prompt user to enter the sum of total charges
6. Prompt user to enter the sum of total credits
7. Prompt user to enter the sum of credit limit
8. Make function to be sure that entered variables are valid /are positive numbers/
9. Make calculation "beginning balance + total charges - total credits"
10. If the calculation value is less than credit limit print only new balance, else print new balance, account number, balance value and info "Credit limit exceeded"
11. Enter -1 to exit the program
