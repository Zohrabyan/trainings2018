/// Exercise 4.26
/// Program that initialize palindrome number
#include <iostream>
#include <unistd.h>

int main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please enter 5 digit number to initialize palindrome number: " << std::endl; /// prompt user to input value
    }
    int palindrome; /// declare variable palindrome
    std::cin >> palindrome;  /// input
    if (palindrome < 10000) {
        std::cout << "Error 1: Invalid number: " << std::endl;  /// end the program with error if inputed value < 1
        return 1;
    }
    if (palindrome > 99999) {
        std::cout << "Error 2: Invalid number: " << std::endl;  /// end the program with error if inputed value > 20
        return 2;
    }
    int counter = palindrome;
    int calculator = 0;
    while (counter != 0) {
        int digit = counter % 10;
        calculator = calculator * 10 + digit;
        counter /= 10;
    }
    if (palindrome == calculator) {  /// statement to calculate entered number backward
        std::cout << "Entered number " << palindrome << " is palindrome. " << std::endl;
    } else {
        std::cout << "Entered number " << palindrome << " is not palindrome. " << std::endl;
    }
    return 0; /// indicate successful termination
} /// end main

