/// Exercise 4.32
/// Program that input 3 double variable numbers and prints
/// whether they could represent the sides of a triangle
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        /// prompt user to enter integers as sizes of triangle
        std::cout << "Please enter integers for sizes of triangle sides " << std::endl;
    }
    double side1;
    std::cin >> side1;
    if (side1 <= 0) {
        /// if entered integer is negative stop the program and print error message
        std::cout << "Error 1: Invalid number, the side can't be negative! " << std::endl;
        return 1;
    }
    double side2;
    std::cin >> side2;
    if (side2 <= 0) {
        std::cout << "Error 1: Invalid number, the side can't be negative! " << std::endl;
        return 1;
    }
    double side3;
    std::cin >> side3;
    if (side3 <= 0) {
        /// measure the size specification of triangle where a side lenghth can't be longer than the sum of 2 other sides 
        std::cout << "Error 1: Invalid number, the side can't be negative! " << std::endl;
        return 1;
    }  
    if ((side1 + side2) > side3) {
        if ((side1 + side3) > side2) {
            if ((side2 + side3) > side1) {
                std::cout << "Sides can represent a triangle " << std::endl;
                return 0; /// indicate successful termination for correct result
            }
        }
    }
    std::cout << "Sides can't represent a triangle! " << std::endl;
    return 0; /// indicate successful termination for wrong result
} /// end main

