///Exercise 02_16
///Write a program that asks the user to enter two numbers, obtains the two numbers from the user and prints the sum , product, difference, and quotient of the two numbers.

///Answer:

#include <iostream> ///standart input an output

int
main()
{
    int number1, number2;

    std::cout << "Please enter first number: ";
    std::cin >> number1;

    std::cout << "Please enter second number: ";
    std::cin >> number2;

    int sum = number1 + number2;

    std::cout << "Sum = " << sum << std::endl;

    int product = number1 * number2;

    std::cout << "Product = " << product << std::endl;

    int difference = number1 - number2;

    std::cout << "Difference = " << difference << std::endl;

    if (0 == number2) {
        std::cout << "Error 1: A number can't be divided to zero " << std::endl;
        return 1;
    }

    int quotient = number1 / number2;

    std::cout << "Quotient = " << quotient << std::endl;

    return 0;
}
