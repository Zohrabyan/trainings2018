Exercise 1.9

Give a brief answer to each of the following questions:
a. Why does this text discuss structured programming in addition to object-oriented programming?
b. What are the typical steps (mentioned in the text) of an object-oriented design process?
c. What kinds of messages do people send to one another?
d. Objects send messages to one another across well-defined interfaces. What interfaces does a car radio (object) present to its user (a person object)?

Answer:

а. They are related together... Diversity never hurts
b. Programm writing, compiling, building etc.
c. Emails, writing, telephone etc.
d. Buttons, which help to choose a radio station, adjust volume etc.
