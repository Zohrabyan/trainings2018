Questions:
Give a brief answer to each of the following questions:
a. Why does this text discuss structured programming in addition to object-oriented programming?
b. What are the typical steps (mentioned in the text) of an object-oriented design process?
c. What kinds of messages do people send to one another?
d. Objects send messages to one another across well-defined interfaces. What interfaces does a car radio (object) present to its user
(a person object)?
Answers:
a:To show the differences and the advantages of both ways of programming.
b:OOD design process consists of 3 phases, analysis, design, implementation.
c:People usually avoid long texts or phrases, they mainly text messages a few words long.
d:Touch screen interfaces & Button and screen interfaces.