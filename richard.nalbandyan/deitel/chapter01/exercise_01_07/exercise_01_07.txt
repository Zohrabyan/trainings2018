Question:
Why is so much attention today focused on object-oriented programming in general and C++ in particular?
Answer:
Object-oriented programs are easier to understand, correct and modify.They can also be more productive than previous popular programming techniques.