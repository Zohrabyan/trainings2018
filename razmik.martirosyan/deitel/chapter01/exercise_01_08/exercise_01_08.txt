Fatal error is an error that causes a program to abort with error number.
A non-fatal error is one where something messes up, but the program can continue to do what it's supposed to do but with logical error.
Fatal error more desirable because you can easy find and eliminate error.
